# TwitterRestJavaClient
Create a module of java code that searches the twitter API for a term at regular intervals (ie every hour) and stores the results.  The results should be queryable, in English, and filtered according to whether the query term appears embedded in a word, in a hashtag, within the tweet text, in the username.

**Pre Requisite**

    * Create a twitter account if you haven't already got one

    * Go to https://dev.twitter.com/apps and create a new test app

    * Generate your access token


Under OAuth Tool you should now have the following values:

1. Consumer key

2. Consumer secret

3. Access token

4. Access token secret

.
You now have the prerequisites for the development.