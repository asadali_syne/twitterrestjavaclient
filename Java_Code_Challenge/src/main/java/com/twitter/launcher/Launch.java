package com.twitter.launcher;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.twitter.util.Application;
import com.twitter.util.Twitter;
import com.twitter.util.LauncherData.Constants;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class Launch {
	private static Map<String, String> applicationProperties = null;
	private static Logger REPORT = null;
	private static Logger ERROR = null;
	private static Twitter twitter = null;
	static {
		applicationProperties = Application.getApplicationProperties(new File(
				Constants.APPFILE));
		REPORT = Logger.getLogger("reportLog");
		ERROR = Logger.getLogger("errorLog");
		twitter = Twitter.getInstance(applicationProperties);
	}

	public static void main(String[] args) {
		Date started = Application.started();
		REPORT.info("Query is being executed for :" +applicationProperties.get(Constants.QUERY));
		twitter.executeQuery();
		Application.ended(started);
	}

}
