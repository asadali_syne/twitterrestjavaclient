package com.twitter.util;


public class LauncherData {
	
		public static class Constants {

			public static final String CONSUMERKEY = "sync.twitter.ConsumerKey";
			public static final String CONSUMERSECRET = "sync.twitter.ConsumerSecret";
			public static final String ACCESSTOKEN = "sync.twitter.AccessToken";
			public static final String APPFILE = "application.properties";
			public static final String METHOD_DELETE = "DELETE";
			public static final String METHOD_GET = "GET";
			public static final String METHOD_POST = "POST";
			public static final String METHOD_PUT = "PUT";
			public static final String ACCESSTOKENSECRET = "sync.twitter.AccessTokenSecret";
			public static final String QUERY="sync.twitter.filter";
			public static final String DATEFORMAT = "yyyy/MM/dd hh:mm a";
			public static final String TIMEZONE = "IST";
			public static final String LASTRUN = "sync.twitter.lastrun";

}
}
