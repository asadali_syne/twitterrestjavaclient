package com.twitter.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;

import org.apache.log4j.Logger;

import com.google.common.collect.Maps;
import com.twitter.util.LauncherData.Constants;


public class Application {
	static Properties properties = new Properties();
	public static final Logger REPORT = Logger.getLogger("reportLog");
	public static final Logger ERROR = Logger.getLogger("errorLog");
	public static Map<String, String> getApplicationProperties(File file) {

		Map<String, String> param = new HashMap<String, String>();
		InputStream stream = null;
		try {
			stream = new FileInputStream(file.getAbsolutePath());

			properties.load(stream);
			param = Maps.fromProperties(properties);
		} catch (IOException e) {
			ERROR.info(Level.INFO, e);
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				ERROR.info(Level.INFO, e);

			}
		}
		return param;
	}
	public static Date started() {
		Date started=new Date();
		DateFormat simpleDateFormat= new SimpleDateFormat(Constants.DATEFORMAT);
		REPORT.info("Application Started at :"+simpleDateFormat.format(started)+"--------------------------------------------------------------------------------------------------------------------------------------------------------");
		ERROR.info("Application Started at :"+simpleDateFormat.format(started)+"--------------------------------------------------------------------------------------------------------------------------------------------------------");
		return started;
	}
	public static void ended(Date startDate) {
		DateFormat simpleDateFormat= new SimpleDateFormat(Constants.DATEFORMAT);
		Date endDate=new Date();
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone(Constants.TIMEZONE));
		updateAppProperty(startDate);
		String duration=null;
		long difference=(endDate.getTime()-startDate.getTime())/1000;
		duration=getDuration(difference);
		REPORT.info("Total Time taken to complete :"+duration+" ------------------------------------------------------------------------------------------------------------------------------------------------------");
		ERROR.info("\nTotal Time taken to complete :"+duration+" ------------------------------------------------------------------------------------------------------------------------------------------------------");
		
		
	}
	private static String getDuration(long seconds) {
		long hours=seconds/3600L;
		long minutes = seconds%36000L /60L;
		seconds%=60L;
		return twoDigitString(hours)+"h : "+ twoDigitString(minutes)+"m : "+twoDigitString(seconds)+"s";
	
	}
	private static String twoDigitString(long number) {
		if(number==0L )
			return "00";
		if(number/10L==0L )
			return "0" + number;
		return String.valueOf(number);
	}
	private static void updateAppProperty(Date startDate) {
		DateFormat simpleDateFormat= new SimpleDateFormat(Constants.DATEFORMAT);
		setLastUpdated(Constants.LASTRUN,simpleDateFormat.format(startDate));
	}
	private static void setLastUpdated(String lastrun, String format) {
		if(lastrun!=null){
			properties.setProperty(lastrun, format);
			File updatedFile=new File("application.properties");
			try {
				FileOutputStream fileOut= new FileOutputStream(updatedFile);
				properties.store(fileOut, lastrun);
				fileOut.close();
			} catch (FileNotFoundException e) {
				
				ERROR.info(Level.INFO, e);
			} catch (IOException e) {
				
				ERROR.info(Level.INFO, e);
			}
		}
		
	}

}
