package com.twitter.util;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

import com.twitter.util.LauncherData.Constants;

public class Twitter {
	private static Twitter twitter;
	private Map<String, String> applicationProperties;
	private static Logger REPORT = Logger.getLogger("reportLog");
	private static Logger ERROR = Logger.getLogger("errorLog");

	public Twitter(Map<String, String> applicationProperties) {

		this.applicationProperties = applicationProperties;
	}

	public static Twitter getInstance(Map<String, String> applicationProperties) {
		if (twitter == null) {
			return new Twitter(applicationProperties);
		} else
			return twitter;
	}

	public void executeQuery() {
		ConfigurationBuilder configBuilder = new ConfigurationBuilder();
		configBuilder
				.setDebugEnabled(true)
				.setOAuthConsumerKey(
						applicationProperties.get(Constants.CONSUMERKEY))
				.setOAuthConsumerSecret(
						applicationProperties.get(Constants.CONSUMERSECRET))
				.setOAuthAccessToken(
						applicationProperties.get(Constants.ACCESSTOKEN))
				.setOAuthAccessTokenSecret(
						applicationProperties.get(Constants.ACCESSTOKENSECRET));

		Query query = new Query(applicationProperties.get(Constants.QUERY));
		TwitterFactory twitterFactory = new TwitterFactory(
				configBuilder.build());
		twitter4j.Twitter tweets = twitterFactory.getInstance();
		QueryResult queryResult = null;
		try {
			queryResult = tweets.search(query);
		} catch (TwitterException e) {

			ERROR.info(Level.INFO, e);
		}
		List<Status> statuses = null;
		statuses = queryResult.getTweets();
		for (Status status : statuses) {
			REPORT.info(status.getUser().getScreenName() + "....."
					+ status.getUser().getName() + "....." + status.getText()
					+ "...." + status.getCreatedAt());
		}

	}

}
